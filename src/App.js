import React from 'react';
import './App.css';


const tasks = [
  {
    text: "Arrumar o quarto",
    key: 0,
    done: false
  },
  {
    text: "Estudar react",
    key: 1,
    done: false
  },
  {
    text: "Estudar git",
    key: 2,
    done: false
  },
  {
    text: "Estudar HTML e CSS",
    key: 3,
    done: false
  },
]

function App() {
  return (
    <div className="App">
      <header>
        <h1>O que fazer?</h1>
      </header>
      <main>
        <form className="todoInput">
          <input type="text" />
          <br/>
          <button>Enviar</button>
        </form>
        <ul>
          {tasks.map((i) => 
            <li key={i.key}>{i.text}</li>
            )}
        </ul>
      </main>
    </div>
  );
}

export default App;
